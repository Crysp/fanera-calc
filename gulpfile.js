var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('css', function () {
    return gulp.src('./src/sass/style.scss')
        .pipe(sass())
        .pipe(gulp.dest('./build/css'));
});

gulp.task('css:w', function () {
    gulp.watch('./src/sass/*.scss', ['css']);
});