var KINDS = [
    { id: 1, name: '1/2' },
    { id: 2, name: '2/2' },
    { id: 3, name: '2/3' },
    { id: 4, name: '2/4' },
    { id: 5, name: '3/3' },
    { id: 6, name: '3/4' },
    { id: 7, name: '4/4' },
    { id: 8, name: 'Каркасная ТУ' }
];

/**
 * Марка
 */
var MarkModel = Backbone.Model.extend({

    defaults: {
        name: null
    },

    initialize: function (attributes, options) {
        options || (options = {});
        this.formats = new FormatCollection;
        this.depths = new DepthCollection;
    }

});

var MarkCollection = Backbone.Collection.extend({

    model: MarkModel

});

/**
 * Форматы
 */
var FormatCollection = Backbone.Collection.extend({

    model: Backbone.Model.extend({ defaults: { width: 0, height: 0 } })

});

/**
 * Толщины
 */
var DepthCollection = Backbone.Collection.extend({

    model: Backbone.Model.extend({ defaults: { value: 0 } })

});

/**
 * Сорта
 */
var KindCollection = Backbone.Collection.extend({

    model: Backbone.Model.extend({ defaults: { name: null } })

});

/**
 * Модель фанеры для распила
 */
var PlywoodModel = Backbone.Model.extend({

    defaults: {
        mark: null,
        format: null,
        kind: null,
        depth: null,
        amount: 0
    },

    initialize: function (attributes, options) {
        options || (options = {});
        attributes || (attributes = {});
        this.figures = new PlywoodFigureCollection(attributes.figures);
        this.listenTo(this.figures, 'add', this.onAddFigure.bind(this));
        this.listenTo(this.figures, 'remove', this.onRemoveFigure.bind(this));
    },

    onAddFigure: function (model) {
        var width = model.get('width');
        var height = model.get('height');
        var depth = this.get('depth');
        var amount = this.get('amount') + (width + height) * 2 + depth * 10;
        this.set('amount', amount);
        Backbone.Events.trigger('plywood:state-change');
    },

    onRemoveFigure: function (model) {
        var width = model.get('width');
        var height = model.get('height');
        var depth = this.get('depth');
        var amount = this.get('amount') - ((width + height) * 2 + depth * 10);
        this.set('amount', amount);
        Backbone.Events.trigger('plywood:state-change');
    }

});

/**
 * Модель фигуры на листе фанеры
 */
var PlywoodFigureModel = Backbone.Model.extend({

    defaults: {
        width: 0,
        height: 0,
        count: 0
    }

});

/**
 * Коллекция листов фанеры
 */
var PlywoodCollection = Backbone.Collection.extend({

    model: PlywoodModel,

    initialize: function (models, options) {
        this.on('add', this.saveLocal.bind(this));
        this.listenTo(Backbone.Events, 'plywood:add', function (model) {
            this.add(model);
            this.saveLocal();
        });
        this.listenTo(Backbone.Events, 'plywood:state-change', this.saveLocal.bind(this));
        this.on('reset', this.clearLocal.bind(this));
    },

    saveLocal: function () {
        var data = this.toJSON();
        this.each(function (model, i) {
            data[i].figures = model.figures.toJSON()
        });
        console.log('save');
        localStorage.setItem('plywood:store', JSON.stringify(data));
    },

    clearLocal: function () {
        localStorage.setItem('plywood:store', JSON.stringify([]));
    }

});


/**
 * Коллекция фигур
 */
var PlywoodFigureCollection = Backbone.Collection.extend({

    model: PlywoodFigureModel

});

var Marks = new MarkCollection([
    { id: 1, name: 'Фанера ФК' },
    { id: 2, name: 'Фанера ФСФ' },
    { id: 3, name: 'Фанера ФБС-1' }
]);
var Kinds = new KindCollection([
    { id: 1, name: '1/2' },
    { id: 2, name: '2/2' },
    { id: 3, name: '2/3' },
    { id: 4, name: '2/4' },
    { id: 5, name: '3/3' },
    { id: 6, name: '3/4' },
    { id: 7, name: '4/4' },
    { id: 8, name: 'Каркасная ТУ' }
]);

Marks.findWhere({ name: 'Фанера ФК' }).formats.add([
    { id: 1, width: 1525, height: 1525 }
]);

Marks.findWhere({ name: 'Фанера ФК' }).depths.add([
    { id: 1, value: 3 },
    { id: 2, value: 4 },
    { id: 3, value: 6 },
    { id: 4, value: 8 },
    { id: 5, value: 9 },
    { id: 6, value: 10 },
    { id: 7, value: 12 },
    { id: 8, value: 15 },
    { id: 9, value: 18 },
    { id: 10, value: 20 },
    { id: 11, value: 21 }
]);

Marks.findWhere({ name: 'Фанера ФСФ' }).formats.add([
    { width: 1220, height: 2440 },
    { width: 1250, height: 2500 },
    { width: 1500, height: 3000 },
    { width: 1525, height: 3050 }
]);

Marks.findWhere({ name: 'Фанера ФСФ' }).depths.add([
    { value: 4 },
    { value: 6 },
    { value: 9 },
    { value: 12 },
    { value: 15 },
    { value: 18 },
    { value: 21 },
    { value: 24 },
    { value: 27 },
    { value: 30 },
    { value: 35 },
    { value: 40 }
]);

Marks.findWhere({ name: 'Фанера ФБС-1' }).formats.add([
    { width: 1525, height: 1525 }
]);

Marks.findWhere({ name: 'Фанера ФБС-1' }).depths.add([
    { value: 10 },
    { value: 12 },
    { value: 14 },
    { value: 15 },
    { value: 16 },
    { value: 18 },
    { value: 20 }
]);

/**
 * Список листов фанеры (таблица)
 */
var PlywoodListView = Backbone.View.extend({

    el: '[data-js="plywood-container"]',

    initialize: function () {
        var stored = JSON.parse(localStorage.getItem('plywood:store'));
        // все листы
        this.plywoods = new PlywoodCollection(stored);
        this.listenTo(this.plywoods, 'add', this.render.bind(this));
        this.listenTo(this.plywoods, 'remove', this.render.bind(this));
        this.listenTo(this.plywoods, 'reset', this.render.bind(this));
    },

    render: function () {
        if (this.plywoods.length) this.$el.html('');
        else this.$el.html('<tr><td colspan="7" class="text-center">Пусто</td></tr>');
        this.plywoods.each(function (model) {
            this.renderPlywood(model);
        }.bind(this));
    },

    renderPlywood: function (model) {
        this.$el.append(new PlywoodItemView({
            model: model
        }).render());
    }

});

/**
 * Строка с отдельной фанерой
 */
var PlywoodItemView = Backbone.View.extend({

    tagName: 'tr',

    template: _.template($('[data-js="template-plywood-item"]').html()),

    events: {
        'click [data-js="plywood-calculate"]': 'onCalculate',
        'click [data-js="plywood-remove"]': 'onRemove',
        'click [data-js="plywood-figure-control-add"]': 'onAddFigure',
        'click [data-js="plywood-figure-control-remove"]': 'onRemoveFigure',
        'click [data-js="plywood-figure-control-reverse"]': 'onReverseFigure',
        'change [data-js="plywood-control-marks"]': 'onChangeMark',
        'change [data-js="plywood-control-kinds"]': 'onChangeKind',
        'change [data-js="plywood-control-formats"]': 'onChangeFormat',
        'change [data-js="plywood-control-depths"]': 'onChangeDepth'
    },

    initialize: function (options) {
        options || (options = {});
        this.model = options.model || new PlywoodModel;
        this.selectedMark = Marks.findWhere({ name: this.model.get('mark') }) || Marks.models[0];
        this.listenTo(this.model.figures, 'add', this.render);
        this.listenTo(this.model.figures, 'remove', this.render);
        this.listenTo(this.model.figures, 'change', this.render);
        this.listenTo(this.model, 'change:amount', this.render);
    },

    render: function () {
        this.$el.html(this.template(_.extend(this.model.toJSON(), {
            marks: Marks.toJSON(),
            formats: this.selectedMark.formats.toJSON(),
            depths: this.selectedMark.depths.toJSON(),
            kinds: Kinds.toJSON(),
            figures: this.model.figures.toJSON()
        })));
        this.$el.attr('data-js', 'plywood-row');
        return this.el;
    },

    onChangeMark: function (ev) {
        this.selectedMark = Marks.findWhere({ name: $(ev.currentTarget).find('option:selected').text() });
        this.model.set('mark', this.selectedMark.get('name'));
        this.render();
        Backbone.Events.trigger('plywood:state-change');
    },

    onChangeKind: function (ev) {
        this.model.set('kind', $(ev.currentTarget).find('option:selected').text());
        Backbone.Events.trigger('plywood:state-change');
    },

    onChangeFormat: function (ev) {
        this.model.set('format', $(ev.currentTarget).find('option:selected').text());
        Backbone.Events.trigger('plywood:state-change');
    },

    onChangeDepth: function (ev) {
        this.model.set('depth', parseFloat($(ev.currentTarget).find('option:selected').text()));
        Backbone.Events.trigger('plywood:state-change');
    },

    onAddFigure: function (ev) {
        ev.preventDefault();
        var width = parseFloat(this.$el.find('[data-js="plywood-figure-width"]').val()) || 0;
        var height = parseFloat(this.$el.find('[data-js="plywood-figure-height"]').val()) || 0;
        var count = parseFloat(this.$el.find('[data-js="plywood-figure-count"]').val()) || 0;
        if (!width && !height && !count) return false;
        var figure = this.model.figures.find({
            width: width,
            height: height
        });
        if (figure) {
            figure.set('count', (figure.get('count') + count));
        } else {
            this.model.figures.add({
                width: width,
                height: height,
                count: count
            });
        }
        Backbone.Events.trigger('plywood:state-change');
    },

    onRemoveFigure: function (ev) {
        ev.preventDefault();
        var width = parseFloat($(ev.currentTarget).attr('data-width'));
        var height = parseFloat($(ev.currentTarget).attr('data-height'));
        var figure = this.model.figures.find({
            width: width,
            height: height
        });
        figure.destroy();
    },

    onReverseFigure: function (ev) {
        ev.preventDefault();
        var width = parseFloat($(ev.currentTarget).attr('data-width'));
        var height = parseFloat($(ev.currentTarget).attr('data-height'));
        var figure = this.model.figures.find({
            width: width,
            height: height
        });
        figure.set({
            width: height,
            height: width
        });
    },

    onCalculate: function () {
        Backbone.Events.trigger('plywood:calculate', this.model);
    },

    onRemove: function () {
        this.model.destroy();
        this.remove();
        Backbone.Events.trigger('plywood:state-change');
    }

});

/**
 * Форма добавления нового листа
 */
var PlywoodControlView = Backbone.View.extend({

    el: '[data-js="plywood-controls"]',

    template: _.template($('[data-js="template-plywood-controls"]').html()),

    initialize: function (options) {
        options || (options = {});

        this.marks = options.marks;
        this.kinds = options.kinds;
        this.selected = this.marks.models[0];

        this.selectedFormat = null;
        this.selectedKind = null;
        this.selectedDepth = null;

        this.$el.on('change', '[data-js="plywood-control-format"]', function (ev) {
            this.selectedFormat = $(ev.currentTarget).find('option:selected').text();
        }.bind(this));
        this.$el.on('change', '[data-js="plywood-control-kind"]', function (ev) {
            this.selectedKind = $(ev.currentTarget).find('option:selected').text();
        }.bind(this));
        this.$el.on('change', '[data-js="plywood-control-depth"]', function (ev) {
            this.selectedDepth = $(ev.currentTarget).find('option:selected').text();
        }.bind(this));
        this.$el.on('change', '[data-js="plywood-control-marks"]', this.onChangeMark.bind(this));
        this.$el.on('click', '[data-js="plywood-control-add"]', this.onClickAdd.bind(this));
    },

    render: function () {
        this.$el.html(this.template({
            selected: this.selected.id,
            marks: this.marks.toJSON(),
            kinds: this.kinds.toJSON(),
            depths: this.selected.depths.toJSON(),
            formats: this.selected.formats.toJSON()
        }));

        this.selectedFormat = $('[data-js="plywood-control-format"] option:first').text();
        this.selectedKind = $('[data-js="plywood-control-kind"] option:first').text();
        this.selectedDepth = $('[data-js="plywood-control-depth"] option:first').text();
    },

    onChangeMark: function (ev) {
        this.selected = this.marks.get(ev.currentTarget.value);
        this.render();
    },

    onClickAdd: function (ev) {
        ev.preventDefault();
        Backbone.Events.trigger('plywood:add', {
            mark: this.selected.get('name'),
            format: this.selectedFormat,
            kind: this.selectedKind,
            depth: parseFloat(this.selectedDepth)
        });
    }

});

/**
 * Форма добавления новой фигуры
 */
var PlywoodFiguresControlView = Backbone.View.extend({

    el: '[data-js="plywood-figure-controls"]',

    template: _.template($('[data-js="template-plywood-figure-controls"]').html()),

    events: {
        'click [data-js="plywood-figure-control-add"]': 'onClickAdd'
    },

    initialize: function (options) {
        this.model = new PlywoodFigureModel;
    },

    render: function () {
        this.$el.html(this.template());
    },

    onClickAdd: function (ev) {
        ev.preventDefault();
        this.model.set({
            width: parseFloat(this.$el.find('[data-js="plywood-figure-width"]').val()),
            height: parseFloat(this.$el.find('[data-js="plywood-figure-height"]').val()),
            count: parseFloat(this.$el.find('[data-js="plywood-figure-count"]').val())
        });
        Backbone.Events.trigger('plywood-figure:add', this.model.toJSON());
    }

});

var CanvasView = Backbone.View.extend({

    el: '[data-js="plywood-canvas"]',

    initialize: function () {
        // тут можно хранить все канвасы
        this.canvases = [];
        // масштаб
        this.ratio = 5;
        this.listenTo(Backbone.Events, 'plywood:draw', this.render.bind(this));
    },

    renderPlywood: function (width, height) {
        var canvas = document.createElement('canvas');
        canvas.id = 'plywood-' + this.canvases.length;
        canvas.className = 'plywood-canvas';
        canvas.width = width;
        canvas.height = height;
        this.$el.append(canvas);
        this.canvases.push({
            obj: canvas,
            leftLine: 0,
            topLine: 0,
            bottomLine: 0
        });
        return this.canvases[this.canvases.length - 1];
    },

    renderFigures: function (canvas, figures) {
        var context = canvas.obj.getContext('2d');
        figures.each(function (figure) {
            var width = figure.get('width') / this.ratio;
            var height = figure.get('height') / this.ratio;
            for (var i = 0; i < figure.get('count'); i++) {
                // new line
                if (canvas.leftLine + width > canvas.obj.width && canvas.leftLine + height > canvas.obj.width) {
                    canvas.leftLine = 0;
                    canvas.topLine = canvas.bottomLine;
                }
                // если перевернутую фигуру можно поместить на текущей линии, то надо ее перевернуть
                if (canvas.leftLine + width > canvas.obj.width && canvas.leftLine + height < canvas.obj.width) {
                    var tmp = width;
                    width = height;
                    height = tmp;
                }
                // new canvas
                if (canvas.topLine + height > canvas.obj.height && canvas.topLine + width > canvas.obj.height) {
                    canvas = this.renderPlywood(canvas.obj.width, canvas.obj.height);
                    context = canvas.obj.getContext('2d');
                }
                // если перевернутую фигуру можно поместить на текущем листе, то надо ее перевернуть
                if (canvas.topLine + height > canvas.obj.height && canvas.topLine + width < canvas.obj.height) {
                    var tmp = width;
                    width = height;
                    height = tmp;
                }
                context.strokeRect(canvas.leftLine, canvas.topLine, width, height);
                if (canvas.topLine + height > canvas.bottomLine) canvas.bottomLine = canvas.topLine + height;
                canvas.leftLine += width;
            }
        }.bind(this));
    },

    render: function (plywoods) {
        this.$el.html('');
        _.each(plywoods.models, function (plywood) {
            var format = plywood.get('format').split(' x ');
            var width = parseFloat(format[0]) / this.ratio;
            var height = parseFloat(format[1]) / this.ratio;
            var canvas = this.renderPlywood(width, height);
            this.renderFigures(canvas, plywood.figures);
        }.bind(this));
    }

});

$(function () {

    var plywoodList = new PlywoodListView();
    var canvasView = new CanvasView;

    plywoodList.render();

    $('[data-js="plywood-clear-store"]').on('click', function (ev) {
        ev.preventDefault();
        plywoodList.plywoods.reset();
        canvasView.$el.html('');
    });

    $('[data-js="plywood-add-row"]').on('click', function (ev) {
        ev.preventDefault();
        plywoodList.plywoods.add(new PlywoodModel({
            mark: Marks.models[0].get('name'),
            format: Marks.models[0].formats.models[0].get('width') + ' x ' + Marks.models[0].formats.models[0].get('height'),
            depth: Marks.models[0].depths.models[0].get('value'),
            kind: Kinds.models[0].get('name')
        }));
    });

    $('[data-js="plywood-draw"]').on('click', function (ev) {
        ev.preventDefault();
        Backbone.Events.trigger('plywood:draw', plywoodList.plywoods);
    });

});